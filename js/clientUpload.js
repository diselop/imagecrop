$(window).load(function () {
    $('#loading').hide();
    $('#uploader').hide();
    $('#propBtn').click(function(){
        $.post(
            baseUrl+'/ajax/properties',
            {
            countCut:$("#count").val(),
            }
            );   
        $("#uploader").show(); 
        $("#startBtn").hide();
        $("#properties").dialog("close");
    });
    $("#selector").bind("slide", function(event, ui){
        $("#amount").val('c '+ui.values[0]+' - '+ui.values[1]+' картинок');
        $.post(
        baseUrl+'/ajax/properties',
            {
            limit:ui.values[1],
            offset:ui.values[0],
            }          
        );
    });
    $('#startBtn').click(function(){
        $("#properties").dialog("open"); return false;
    });
    new AjaxUpload(uploader, {        
        action: baseUrl+'/ajax/upload', 
        onSubmit : function(file, ext){
            $('#loading').show();
            ext=ext.toLowerCase();
            if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
                $.post(
                    baseUrl+'/ajax/messages',
                    {
                    message:100
                    },
                    function error(data){
                    $("#content").append(data);  
                    $('#loading').hide();
                    }
                );
                return false;
            }
        },
        onComplete: function(file, response){
            $('#loading').hide();
            $("#area").append(response);
            $(window).load();
            $("#startBtn").show();
            $("#startBtn").text('Начать заново');
        }
    });
    $('td').click(function(){                    
        $.post(
            baseUrl+'/ajax/cell',
            {
            cell:this.id,
            },
            function cellImage(data){
                var json=JSON.parse(data); 
                if($(json.id).hasClass("open")!=true){
                    $(json.id).append(json.image);   
                    $(json.id).addClass("open");
                    var count=0
                    $('.open').each( function() { 
                        count++;        
                    });
                    if(count==json.count*json.count)alert("Поздравляем вы выиграли!");
                }
            }
        );
    });          
});


