<?php
class Image {
    static function cut($count,$url,$folder){
        $size=  getimagesize($url);
        $width=$size[0];
        $height=$size[1];
        $mime=$size["mime"];
        switch($mime){
                case 'image/jpeg':
                    $functionImage= 'imagejpeg';
                    $functionImageCreate= 'imagecreatefromjpeg';

                break;
                case 'image/gif';
                    $functionImage= "imagegif";
                    $functionImageCreate= 'imagecreatefromgif';

                break;
                case 'image/png':
                    $functionImage= "imagepng";
                    $functionImageCreate= 'imagecreatefrompng';

         }
         $sourseImage=$functionImageCreate($url);
        if($width>400){
            $resizeImage = imagecreatetruecolor(400, $height);
            $source = $functionImageCreate($url);
            imagecopyresized($resizeImage, $source, 0, 0, 0, 0, 400, $height, $width, $height);
            $width=400;
            $functionImage($resizeImage, $url);
            $sourseImage=$functionImageCreate($url);            
        }
        $cutWidth=$width/$count;
        $cutHeight=$height/$count;        
         $format= '.'.str_replace('image/', '',$mime);
         for($x=0;$x<$count;$x++){
                    for($y=0;$y<$count;$y++){
                    $salt=substr(CPasswordHelper::generateSalt(),-2);
                    $cutImage = imagecreatetruecolor($cutWidth, $cutHeight);
                    imagecopy($cutImage, $sourseImage, 0, 0, $cutWidth*$x, $cutHeight*$y, $cutWidth, $cutHeight); 
                    $fileName=$y.$x.$format;
                    $dir='img/'.$folder.'/'.$fileName;
                    $functionImage($cutImage,$dir ); 
                    $cut=new Cut;
                    $cut->parent=$folder;
                    $cut->fileName=$fileName;
                    $cut->key=$salt;
                    $cut->value=$y.$x;
                    $cut->save();                    
                }
         }
        
    }
}
