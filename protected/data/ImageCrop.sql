-- phpMyAdmin SQL Dump
-- version 4.1.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 20 2013 г., 14:19
-- Версия сервера: 5.5.34-0ubuntu0.13.10.1
-- Версия PHP: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `ImageCrop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cut`
--

CREATE TABLE IF NOT EXISTS `cut` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` varchar(10) NOT NULL,
  `fileName` varchar(10) NOT NULL,
  `key` varchar(4) NOT NULL,
  `value` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `cut`
--

INSERT INTO `cut` (`id`, `parent`, `fileName`, `key`, `value`) VALUES
(1, '06a3aef86d', '00.jpeg', 'hJ', '00'),
(2, '06a3aef86d', '10.jpeg', 'P4', '10'),
(3, '06a3aef86d', '20.jpeg', 'wL', '20'),
(4, '06a3aef86d', '30.jpeg', '/O', '30'),
(5, '06a3aef86d', '01.jpeg', 'gt', '01'),
(6, '06a3aef86d', '11.jpeg', '8c', '11'),
(7, '06a3aef86d', '21.jpeg', 'TA', '21'),
(8, '06a3aef86d', '31.jpeg', 'Cl', '31'),
(9, '06a3aef86d', '02.jpeg', 'TF', '02'),
(10, '06a3aef86d', '12.jpeg', '7c', '12'),
(11, '06a3aef86d', '22.jpeg', '8/', '22'),
(12, '06a3aef86d', '32.jpeg', 'RA', '32'),
(13, '06a3aef86d', '03.jpeg', 'Yx', '03'),
(14, '06a3aef86d', '13.jpeg', 'xL', '13'),
(15, '06a3aef86d', '23.jpeg', 'Uj', '23'),
(16, '06a3aef86d', '33.jpeg', 'tR', '33'),
(17, '69ef4d66f1', '00.jpeg', '3l', '00'),
(18, '69ef4d66f1', '10.jpeg', 'wB', '10'),
(19, '69ef4d66f1', '20.jpeg', 'Gz', '20'),
(20, '69ef4d66f1', '01.jpeg', 'Zn', '01'),
(21, '69ef4d66f1', '11.jpeg', 'lU', '11'),
(22, '69ef4d66f1', '21.jpeg', 'Gb', '21'),
(23, '69ef4d66f1', '02.jpeg', '4Y', '02'),
(24, '69ef4d66f1', '12.jpeg', 'c6', '12'),
(25, '69ef4d66f1', '22.jpeg', 'TQ', '22');

-- --------------------------------------------------------

--
-- Структура таблицы `source`
--

CREATE TABLE IF NOT EXISTS `source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(20) NOT NULL,
  `folder` varchar(10) NOT NULL,
  `countSession` tinyint(4) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `source`
--

INSERT INTO `source` (`id`, `fileName`, `folder`, `countSession`, `width`, `height`) VALUES
(1, '86028.jpg', '06a3aef86d', 4, 400, 600),
(2, '000.jpg', '69ef4d66f1', 3, 400, 464);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
