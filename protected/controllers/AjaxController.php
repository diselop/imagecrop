<?php
class AjaxController extends Controller {
    public function filters() {
        return array('ajaxOnly-Upload');
    }
    
    public function actionProperties(){        
        $limit=  Yii::app()->request->getPost("limit");
        $offset=  Yii::app()->request->getPost("offset");
        $count=  Yii::app()->request->getPost("countCut");        
        if(isset($limit))   Yii::app()->session->add("limit", $limit);
        if(isset($offset))Yii::app()->session->add("offset", $offset);        
        if(isset($count))   Yii::app()->session->add("count", $count);        
    }
    
    public function actionCell(){
        $cell=  Yii::app()->request->getPost("cell");
        $folder=Yii::app()->session->get("folder");
        $count=Yii::app()->session->get("count");
        $sql="SELECT CONCAT( 'img/',parent,'/',fileName)as dir FROM `cut` where parent=:parent and value=:cell";
        $command=Yii::app()->db->createCommand($sql);
        $command->bindParam(":parent",$folder);
        $command->bindParam(":cell",$cell);
        $cellImage=  $command->queryScalar();
        $image=CHtml::image($cellImage);
        $json=array('id'=>'#'.$cell,'image'=>$image,'count'=>$count);
        echo json_encode($json);        
    }
    
    public function actionChanger(){        
        $limit=  Yii::app()->request->getPost("limit");
        $offset=  Yii::app()->request->getPost("offset");
        $count=  Yii::app()->request->getPost("countCut");      
    }
    
    public function actionUpload(){   
        $count=Yii::app()->session->get("count");
        $folder=Yii::app()->session->get("folder");
        $limit=Yii::app()->session->get("limit");
        $offset=Yii::app()->session->get("offset");
        isset($limit)?'':$limit=0;
        isset($offset)?'':$offset=0;
        $uploadDir='img/';
        $fileName=$_FILES['userfile']['name'];
        $uploadFile=$uploadDir.basename($_FILES['userfile']['name']);
        if (file_exists($uploadFile)) {
            $user = Yii::app()->getComponent('user');
            $user->setFlash(
                'warning',
                "<strong>Данный файл уже имеется!</strong> загрузите другой."
            );
            $this->widget('bootstrap.widgets.TbAlert', array(
                'id'=>'info',
            ));  
        }
        else{
            if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadFile)) {
                $folder = substr(md5_file($uploadFile), 0,10); 
                $size=  getimagesize($uploadFile);
                $size[0]>400?$width=400:$width=$size[0];
                $height=$size[1];
                mkdir($uploadDir.$folder,0777);
                $source=new Source;
                $source->fileName=$fileName;
                $source->folder=$folder;
                $source->countSession=$count;
                $source->width=$width;
                $source->height=$height;
                $source->save();
                Yii::app()->session->add("id", $source->id);
                Yii::app()->session->add("folder", $folder);
                Image::cut($count, $uploadFile, $folder); 
                $id=$source->id;
                $sql="SELECT value,CONCAT( 'img/',parent,'/',fileName)as dir FROM `cut` where parent=:parent order by value limit $limit offset $offset ";       
                $command=Yii::app()->db->createCommand($sql);
                $command->bindParam(":parent",$folder);
                $cutImage=  $command->queryAll();
                foreach ($cutImage as $cut){
                    $image[$cut['value']]=$cut['dir'];
                }
                $sqlSize='SELECT width,height   FROM `source` where id=:id';
                $command=Yii::app()->db->createCommand($sqlSize);
                $command->bindParam(":id",$id);
                $sizeImg=  $command->queryRow();
                $width=$sizeImg['width'];
                $height=$sizeImg['height'];
                $trHeight=(int)($height/$count);
                $trWidth=(int)($width/$count);
                echo "<table width=$width height=$height>";
                for($i=0;$i<$count;$i++){
                    echo "<tr height=$trHeight>";
                    for($j=0;$j<$count;$j++){
                        if(isset($image[$i.$j])){$img=CHtml::image($image[$i.$j]);
                        $class="class='open'";
                    }
                    else {
                        $img=null; 
                        $class=null;                        
                    }
                    echo "<td $class width=$trWidth id=".$i.$j.">$img</td>";
                    }
                    echo '</tr>';
                }
                echo '</table>';
            }
            else {
                $user = Yii::app()->getComponent('user');
                $user->setFlash(
                    'error',
                    "<strong>Ошибка!</strong>"
                );
                $this->widget('bootstrap.widgets.TbAlert', array(
                    'id'=>'info',
                ));
            }
        }       
    }
    
         
    public function actionMessages(){
        $code=  Yii::app()->request->getPost("message");
        $user = Yii::app()->getComponent('user');
        switch($code){
                case 100:
                $type='error';
                $decription="Неверный формат файла!"; 
                break;
                case 200:
                $type='success';
                $decription="Поздравляем вы выиграли!"; 
                break;
         }         
        $user->setFlash(
            $type,
            "<strong>$decription</strong>"
        );         
         $this->widget('bootstrap.widgets.TbAlert', array(
            'id'=>'message',
        ));      
    }
    
    public function actionTable(){
         $count=Yii::app()->session->get("count");
         $folder=Yii::app()->session->get("folder");
         $limit=Yii::app()->session->get("limit");
         $offset=Yii::app()->session->get("offset");         
    }
    
}
