<?php
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'image',
        'language'=>'ru',
	'preload'=>array('log','bootstrap'),
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			'ipFilters'=>array('127.0.0.1','::1'),
                        'generatorPaths'=>array('bootstrap.gii'),
		),
	),

	'components'=>array(

		'user'=>array(
			'allowAutoLogin'=>true,
		),
	
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
                    'showScriptName'=>false,
		),

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=ImageCrop',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '4heln72i',
			'charset' => 'utf8',
		),
		
		'errorHandler'=>array(
			
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),

			),
		),
                'bootstrap' => array(
                    'class' => 'ext.booster.components.Bootstrap',
                    'responsiveCss'=>true,
                ),
	),



);