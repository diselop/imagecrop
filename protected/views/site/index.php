<?php
Yii::app()->session->add("test", 123);
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'label' => 'Загрузить',
        'type' => 'primary',
        'id'=>'uploader',
    )
);
echo CHtml::image($this->createUrl('img/source/load.gif'),'',$htmlOptions=array('id'=>'loading'));
$this->widget(
    'bootstrap.widgets.TbButton',
    array(
        'label' => 'Начать',
        'type' => 'primary',
        'id'=>'startBtn',
    )
);
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'properties',
    'options'=>array(
    'title'=>'Настройки',
    'autoOpen'=>false,
    'modal'=>'true',
    ),
));
        echo CHtml::label('Выберите размер', 'count');
        echo  CHtml::dropDownList('count','' ,
            array('3' => '3x3', '4' => '4x4')); 
        echo CHtml::label('Открыть картинки', 'selector');
        $this->widget('zii.widgets.jui.CJuiSlider', array(
            'id'=>'selector',
            'options'=>array(
            'min'=>0,
            'max'=>9,
            'range'=>true,
            'values'=>array(0, 0),
            ),
        ));
        echo CHtml::textField('amount', 'ни одной',$htmlOptions=array());
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Сохранить',
                'type' => 'primary',
                'id'=>'propBtn',
            )
        );
$this->endWidget('zii.widgets.jui.CJuiDialog');
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/AjaxUpload.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/js/clientUpload.js');
?>

<div id="area"></div>